from collections import OrderedDict

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APITestCase

from api.models import Publication
from api.serializer import PublicationSerializer


class PublicationApiTest(APITestCase):
    def setUp(self):
        user1 = User.objects.create(username="usertest1", password="passsadsad")
        user2 = User.objects.create(username="usertest2", password="passsadsad")
        self.objects = []
        for i in range(2):
            obj = Publication.objects.create(
                title=str(i), description=str(i), author=user1
            )
            self.objects.append(obj)
        for i in range(2, 4):
            obj = Publication.objects.create(
                title=str(i), description=str(i), author=user2
            )
            self.objects.append(obj)

    def test_publication_list_authentication(self):
        """тут проверяем что доступен только для авторизованного"""
        url = reverse("publication-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_publication_list_serializer(self):
        self.user = User.objects.create_user(
            username="testsuperuser", password="testsuperuser"
        )
        self.client.force_authenticate(user=self.user)
        publication = Publication.objects.create(
            title="testtitle", description="testasddescription", author=self.user
        )
        url = reverse("publication-list")
        serializer = PublicationSerializer(publication)
        serializer = OrderedDict(serializer.data)
        response = self.client.get(url)
        result_response = response.data[0]
        del result_response["rate"]
        """ удалил rate т.к он аннотируется в момент обращения вьюхи, а мы тестируем сериализатор"""
        self.assertEqual(response.data[0], serializer)


class PublicationCalculateApiTest(APITestCase):
    def test_annotate_calculate(self):
        """проверяем что проаннотировалось корректно во вьюхе"""
        self.user = User.objects.create_user(
            username="testsuperuser", password="testsuperuser"
        )
        self.client.force_authenticate(user=self.user)
        url = reverse("publication-list")
        response = self.client.get(url)
        ressponse_data = response.data[0].keys()
        self.assertIn("rate", ressponse_data)


class VoteApiTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username="usertest", password="passsadsad")
        self.client.force_authenticate(user=self.user)
        self.publication = Publication.objects.create(
            title="testtest_vote", description="test_votetest_vote", author=self.user
        )

    def test_post_vote(self):
        url = reverse("publication-vote", kwargs={"pk": self.publication.id})
        response = self.client.post(url, data={"vote_for": "0"})
        self.assertEqual(response.status_code, 200)
        response = self.client.post(url, data={"vote_for": "1"})
        self.assertEqual(response.status_code, 200)
        response = self.client.post(url, data={"vote_for": "2"})
        self.assertEqual(response.status_code, 400)

    def test_delete_vote(self):
        url = reverse('publication-vote', kwargs={'pk': self.publication.id})
        self.client.post(url, data={"vote_for": "0"})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 400)
