from django.db.models import QuerySet, IntegerField, Sum, Case, When

from api.constants import VoteType


class PublicationQuerySet(QuerySet):
    """
    Менеджер публикаций
    """

    def active(self):
        return self.filter(active=True)

    def calculate_rate(self):
        """
        Расчёт рейтинга для публикаций.
        """
        return self.annotate(
            rate=Sum(
                Case(
                   When(votes__vote_for=VoteType.PLUS, then=1),
                   When(votes__vote_for=VoteType.MINUS, then=-1),
                   default=0,
                   output_field=IntegerField()
                )
            )
        )

