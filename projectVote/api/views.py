from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Publication, Votes
from api.serializer import PublicationSerializer, PublicationPostSerializer
from rest_framework.permissions import IsAuthenticated


# @method_decorator(cache_page(1), name='get')
class PublicationAPIView(ListCreateAPIView):
    """АПИ которая отдает 10 самых рейтинговых, последних статей"""

    permission_classes = [IsAuthenticated]
    filter_backends = [OrderingFilter]
    ordering_fields = ['rate', 'created_at']

    def get_queryset(self):
        return (
            Publication.objects.all()
            .calculate_rate()
            .order_by("-created_at", "-rate")
        )

    def get_serializer_class(self):
        if self.request.method == "GET":
            return PublicationSerializer
        elif self.request.method == "POST":
            return PublicationPostSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data["author"] = request.user
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())[:10]

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class VotesCreateUpdateAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk, format=None):
        success_msg = {"result": "Вы успешно проголосовали"}
        error_message = {
            "result": "Ошибка. Необходимо передать vote_for в диапазоне от 0 до 1"
        }
        vote_for = request.data.get("vote_for")
        if vote_for not in ["0", "1"] or not vote_for:
            return Response(data=error_message, status=status.HTTP_400_BAD_REQUEST)
        with transaction.atomic():
            try:
                publication = Publication.objects.get(id=pk)
                vote_on_publication, created = Votes.objects.get_or_create(
                    publication=publication, user=request.user
                )
            except ObjectDoesNotExist as e:
                return Response(
                    f"Публикации с pk {pk} не существует. {e}",
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if vote_on_publication:
                vote_on_publication.vote_for = vote_for
                vote_on_publication.save()
                return Response(data=success_msg, status=status.HTTP_200_OK)
            else:
                return Response(data=error_message, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        success_msg = {"Голос успешно отменён"}
        error_msg = {"Пользователь не голосовал"}
        with transaction.atomic():
            try:
                publication = Publication.objects.get(id=pk)
                vote = Votes.objects.filter(publication=publication, user=request.user)
                if vote:
                    vote.delete()
                    return Response(data=success_msg, status=status.HTTP_200_OK)
                else:
                    return Response(data=error_msg, status=status.HTTP_400_BAD_REQUEST)
            except ObjectDoesNotExist as e:
                return Response(
                    f"Публикации с pk {pk} не существует. {e}",
                    status=status.HTTP_400_BAD_REQUEST,
                )
