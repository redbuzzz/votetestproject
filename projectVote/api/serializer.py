from rest_framework import serializers

from api.models import Publication, Votes


class PublicationSerializer(serializers.ModelSerializer):
    rate = serializers.IntegerField(read_only=True)

    class Meta:
        model = Publication
        fields = ['id', 'title', 'description', 'rate', 'author', 'created_at', 'updated_at']


class PublicationPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ['id', 'title', 'description', 'author']
        read_only_fields = ['id', 'author', 'created_at']

    class VotesCreateSerializer(serializers.ModelSerializer):
        class Meta:
            model = Votes
            fields = '__all__'
