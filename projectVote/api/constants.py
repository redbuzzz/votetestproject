from django.db.models import IntegerChoices


class VoteType(IntegerChoices):
    PLUS = 0, "+"
    MINUS = 1, "-"
