from django.contrib.auth.models import User
from django.db import models

from api.constants import VoteType
from api.querysets.publication_queryset import PublicationQuerySet


class Publication(models.Model):
    objects = PublicationQuerySet.as_manager()
    active = models.BooleanField('Показывать публикацию', default=True)
    title = models.CharField('Заголовок статьи', max_length=70)
    description = models.TextField('Текст статьи', max_length=8000)
    created_at = models.DateTimeField('Дата создания статьи', auto_now_add=True)
    updated_at = models.DateTimeField('Дата последнего обновления статьи', auto_now=True)
    author = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='author',
                               verbose_name='Автор публикации')

    class Meta:
        ordering = ('-created_at',)
        verbose_name = 'Публикация'
        verbose_name_plural = 'Публикации'

    def __str__(self):
        return str(self.title)


class Votes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='users')
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE, related_name='votes')
    vote_for = models.IntegerField('Голос за или против', choices=VoteType.choices, default=VoteType.PLUS)

    class Meta:
        verbose_name = 'Голос'
        verbose_name_plural = 'Голоса'
        unique_together = ('user', 'publication')

    def __str__(self):
        return str(self.publication)
